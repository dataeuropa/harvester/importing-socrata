# ChangeLog

## 1.2.1 (2021-06-05)

**Changed:**
* Lib dependencies

## 1.2.0 (2021-01-31)

**Changed:**
* Switched to Vert.x 4.0.0

## 1.1.1 (2020-06-18)

**Changed:**
* Serialize pipe startTime as ISO standard string

## 1.1.0 (2020-05-13)

**Added:**
* Setting filters via pipe config

**Fixed:**
* Recursive requests use copy

## 1.0.0 (2020-04-24)

Initial production release