package io.piveau.consus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.pipe.PipeContext;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static io.piveau.consus.ApplicationConfig.*;

public class ImportingSocrataVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.socrata.queue";

    private static final Logger log = LoggerFactory.getLogger(ImportingSocrataVerticle.class);

    private WebClient client;

    private int defaultDelay;
    private int pageSize;
    private String socrataApiToken;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx);

        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add(ENV_PIVEAU_IMPORTING_SEND_LIST_DELAY)
                        .add(ENV_PIVEAU_PAGE_SIZE)
                        .add(ENV_SOCRATA_API_TOKEN)));

        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions));

        retriever.getConfig()
                .onSuccess(config -> {
                    defaultDelay = config.getInteger(ENV_PIVEAU_IMPORTING_SEND_LIST_DELAY, DEFAULT_PIVEAU_IMPORTING_SEND_LIST_DELAY);
                    pageSize = config.getInteger(ENV_PIVEAU_PAGE_SIZE, DEFAULT_PIVEAU_PAGE_SIZE);
                    socrataApiToken = config.getString(ENV_SOCRATA_API_TOKEN);

                    startPromise.complete();

                })
                .onFailure(startPromise::fail);

        retriever.listen(change ->
                defaultDelay = change.getNewConfiguration().getInteger(ENV_PIVEAU_IMPORTING_SEND_LIST_DELAY, DEFAULT_PIVEAU_IMPORTING_SEND_LIST_DELAY));
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        JsonObject config = pipeContext.getConfig();
        pipeContext.log().info("Import started.");

        String address = config.getString("address");

        // Socrata doesn't accept offset values for result sets > 10000
        // Instead, the ID of the last dataset is used as the scroll_id
        // Example: /api/catalog/v1?limit=10&scroll_id=4kxg-jezx
        HttpRequest<Buffer> request = client.getAbs(address + "/api/catalog/v1")
                .setQueryParam("limit", String.valueOf(pageSize))
                .expect(ResponsePredicate.SC_OK);

        if (socrataApiToken != null)
            request.headers().add("X-App-Token", socrataApiToken);

        if (config.containsKey("filters")) {
            JsonObject filters = config.getJsonObject("filters");
            filters.getMap().forEach((key, value) -> request.addQueryParam(key, value.toString()));
        }

        fetchPage(request.copy(), null, new ArrayList<>(), pipeContext);
    }

    private void fetchPage(HttpRequest<Buffer> request, String scrollId, List<String> identifiers, PipeContext pipeContext) {
        if (scrollId != null)
            request.setQueryParam("scroll_id", scrollId);

        log.info("Fetching datasets starting from [{}]", scrollId);

        request.send()
                .onSuccess(response -> {
                    JsonArray results = response
                            .bodyAsJsonObject()
                            .getJsonArray("results");

                    if (!results.isEmpty()) {
                        results.forEach(entry -> {
                            String identifier = ((JsonObject) entry)
                                    .getJsonObject("resource")
                                    .getString("id");

                            if (identifiers.contains(identifier))
                                pipeContext.log().warn("Duplicate dataset: {} ", identifier);

                            identifiers.add(identifier);

                            ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                                    .put("total", response.bodyAsJsonObject().getInteger("resultSetSize"))
                                    .put("counter", identifiers.size())
                                    .put("identifier", identifier)
                                    .put("catalogue", pipeContext.getConfig().getString("catalogue"));

                            pipeContext.setResult(((JsonObject) entry).encode(), "application/json", dataInfo).forward();
                            pipeContext.log().info("Data imported: {}", dataInfo);
                        });

                        // the last dataset retrieved must be used as the next scrollId
                        String nextScrollId = results
                                .getJsonObject(results.size() - 1)
                                .getJsonObject("resource")
                                .getString("id");

                        fetchPage(request.copy(), nextScrollId, identifiers, pipeContext);
                    } else {
                        pipeContext.log().info("Imported {} datasets", identifiers.size());

                        int delay = pipeContext.getConfig().getInteger("sendListDelay", defaultDelay);
                        vertx.setTimer(delay, t -> {
                            ObjectNode info = new ObjectMapper().createObjectNode()
                                    .put("content", "identifierList")
                                    .put("catalogue", pipeContext.getConfig().getString("catalogue"));

                            pipeContext.setResult(new JsonArray(identifiers).encodePrettily(), "application/json", info).forward();
                        });
                    }
                })
                .onFailure(pipeContext::setFailure);
    }

}
